from flask import Flask, Response, send_file, send_from_directory
from time import sleep

app = Flask(__name__)

@app.route("/usuario/<string:name>")
def isValidName(name):
    if name == 'slow':
        sleep(10)
    resp = Response(str(name == 'ludat'))
    resp.headers['Access-Control-Allow-Origin'] = '*'
    return resp

@app.route("/")
def serveIndex():
    return send_file('./dist/index.html')

@app.route("/<string:path>")
def serveDir(path):
    return send_from_directory('./dist/', path)

if __name__ == "__main__":
    app.run(threaded=True)
