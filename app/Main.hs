module Main where

import Lib

import Reflex.Dom (mainWidget)

main :: IO ()
main = do
  mainWidget htmlMain
