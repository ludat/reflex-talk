{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecursiveDo #-}
{-# LANGUAGE TemplateHaskell #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE TypeFamilies #-}
{-# LANGUAGE ScopedTypeVariables #-}

module Lib
    ( htmlMain
    ) where

import Reflex.Dom
import qualified Data.Text as T
import Text.Read (readMaybe)
import Control.Monad
import qualified GHCJS.DOM.HTMLInputElement as J
-- import Data.FileEmbed

import Data.Text (Text)
import Data.Maybe
import Data.Monoid

insertAt :: Int -> Text -> Text -> (Int, Text)
insertAt n e s = let (u,v) = T.splitAt n s
                  in (n + T.length e, u <> e <> v)

attachSelectionStart :: MonadWidget t m => TextInput t -> Event t a -> m (Event t (Int, a))
attachSelectionStart t ev = performEvent . ffor ev $ \e -> do
  n <- J.getSelectionStart (_textInput_element t)
  return (n,e)

withSetCaret :: MonadWidget t m => Event t Int -> m (TextInput t) -> m (TextInput t)
withSetCaret setCaretE mkTextInput  = do
  t <- mkTextInput
  performEvent_ . ffor setCaretE $ \n -> do
    let ele = _textInput_element t
    J.setSelectionStart ele n
    J.setSelectionEnd ele n
    J.select ele
  return t

-- htmlMain :: MonadWidget t m => m ()
-- htmlMain = do
--   rec insertionE <- fmap leftmost . forM ["abracadabra", "hello", "gg"] $ \s -> fmap (s <$) (button s)
--       insertionLocE <- attachSelectionStart t insertionE
--       let newE = attachWith (\s (n,e) -> insertAt n e s) (current (value t)) insertionLocE
--       t <- withSetCaret (fmap fst newE) . textInput $ def & setValue .~ fmap snd newE
--   -- el "pre" $ text $(embedStringFile "TextInsert.hs")
--   return ()

inputWithSetter setterEvent = textInput $
        def & textInputConfig_setValue .~ setterEvent

celcious2Farenheigh :: Float -> Float
celcious2Farenheigh n = n * 9 / 5 + 32

farenheigh2celcious :: Float -> Float
farenheigh2celcious n = (n - 32) / 9 * 5

c2f :: Reflex t => (Float -> Float) -> Event t Text -> Event t Text
c2f f events =
  fmap (T.pack . show . fromJust) .
  ffilter (isJust) .
  fmap (fmap f . readMaybe . T.unpack) $ events





learningSection :: MonadWidget t m => Text -> m a -> m ()
learningSection name content =
  elClass "div" "learningSection" $ do
    el "h3" $ text name
    void content


















htmlMain :: MonadWidget t m => m ()
htmlMain = do
  learningSection "primero" $ text "cosa"
  learningSection "counter" $ do
    clicks <- button "Clickeame"
    reset <- button "Reset"
    numberDyn <- foldDyn ($) 0 $ mergeWith (.) $
      [ fmap (const (+ 1)) clicks
      , fmap (const (const 0)) reset
      ]
    display numberDyn

  learningSection "clear" $ do
    rec
      inputResult <- inputWithSetter $ fmap (const "") $ textInputGetEnter inputResult
    return ()



















  -- learningSection "Counter" $ do
  --   clicks :: Event t () <- button "Increase"
  --   counter :: Dynamic t Int <- count clicks
  --   display counter

  -- learningSection "temprature converter" $ do
  --   rec
  --     textInputRef1 :: TextInput t <- textInput $
  --       def & textInputConfig_setValue .~ (c2f farenheigh2celcious $ _textInput_input textInputRef2)
  --     textInputRef2 :: TextInput t <- textInput $
  --       def & textInputConfig_setValue .~ (c2f celcious2Farenheigh $ _textInput_input textInputRef1)
  --   return ()

  -- learningSection "Counter" $ do
  --   clicks :: Event t () <- button "Increase"
  --   counter :: Dynamic t Int <- count clicks
  --   display counter
















































  -- learningSection "Requests async rancios" $ do
  --   inputDyn :: Dynamic t Text <- _textInput_value <$> textInput def
  --   buttonEvent <- button "click me"
  --   let defaultReq = fmap (\name -> xhrRequest
  --         "GET" ("http://localhost:5000/usuario/" <> name) def) inputDyn  -- served by snap
  --   response :: Dynamic t Text <-
  --     performRequestAsync (tagPromptlyDyn defaultReq buttonEvent) >>=
  --     holdDyn "" . fmap (fromMaybe "response volvio sin body" . _xhrResponse_responseText)
  --   dynText response

  -- learningSection "Requests async rancios" $ do
  --   inputDyn :: Dynamic t Text <- _textInput_value <$> textInput def
  --   buttonEvent <- button "click me"
  --   let defaultReq = fmap (\name -> xhrRequest
  --         "GET" ("http://localhost:5000/usuario/" <> name) def) inputDyn  -- served by snap
  --   asyncEvent <- performRequestAsync $ tagPromptlyDyn defaultReq buttonEvent
  --   response :: Dynamic t Text <- holdDyn "" $
  --     fmap (fromMaybe "response volvio sin body" . _xhrResponse_responseText) asyncEvent
  --   dynText response

  -- learningSection "Primera cosa" $ do
  --   el "p" $
  --     textInput def
  --   button "Submit"
  -- learningSection "Primera cosa" $ do
  --   rec
  --     let updateText = fmap (const "mas cosas locas") $ ffilter (== "hola") $ updated . value $ t
  --     t <- textInput $ def & setValue .~ updateText
  --   -- el "pre" $ text $(embedStringFile "TextInsert.hs")
  --   return ()


-- htmlMain :: MonadWidget t m => m ()
-- htmlMain = do
--   el "div" $ do
--     p $ do
--       input <- textInput def
--       clicks <- button "Fetch"
--       let
--         isEnter = (== 13)
--         enters = fmap (const ()) $
--           ffilter isEnter $
--           _textInput_keydown input
--         messages = tagPromptlyDyn (_textInput_value input) $
--             leftmost [clicks, enters]

--       socket :: WebSocket t <- webSocket "ws://localhost:8000/connect" $
--         def & webSocketConfig_send .~ (fmap (:[]) messages)
--       -- responseDyn :: Dynamic t Text <- switchPromptlyDyn <$>
--       --   (widgetHold (return never)
--       --     ((\_ -> getAndDecode never) <$> urls))
--       -- responseDyn :: Dynamic t Text <- fmapMaybe id <$> getAndDecode urls
--       --                                  >>= holdDyn "Nothing yet"
--       responseDyn :: Dynamic t [ByteString] <- foldDyn (:) [] $ _webSocket_recv socket

--       text "Welcome to Reflex: "
--       _ <- el "ul" $ simpleList responseDyn $ \m -> el "li" $ dynText $ fmap decodeUtf8 m
--       return ()

